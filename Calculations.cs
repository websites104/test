using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public class Calculations
    {
        public double calculating (string sign, double result, double number1)
        {

            int zmienna = 1;

            switch (sign)
            {
                case "+":
                    {
                        result += number1;
                        break;
                    }
                case "-":
                    {
                        result -= number1;
                        break;
                    }
                case "*":
                    {
                        result *= number1;
                        break;
                    }
                case "÷":
                    {
                        if (number1 != 0)
                        {
                            result /= number1;
                        }
                        break;
                    }
                case "√":
                    {
                        result = Math.Sqrt(number1);
                        break;
                    }
                case "sqr":
                    {
                        result *= result;
                        break;
                    }
                case "!":
                    {
                        for (int i = 1; i <= number1; i++)
                        {
                            zmienna *= i;                          
                        }
                        result = zmienna;
                        break;
                    }
                case "log":
                    {
                        if (number1 > 0)
                        {
                            result = Math.Log10(number1);
                        }
                        else
                        {
                            result = 0;
                        }
                        break;
                    }
                case "%":
                    {
                        result = number1 / 100;
                        break;
                    }

            }

            return result;

        }
      
    }
}